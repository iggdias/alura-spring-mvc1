<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<tags:pageTemplate titulo="Erro genérico!">

	<section id="index-section" class="container middle">
		<h2>O produto informado não foi encontrado!</h2>
	</section>

</tags:pageTemplate>
