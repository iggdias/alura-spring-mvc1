package br.com.casadocodigo.loja.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(Exception.class)	
	public ModelAndView trataExcecaoGenerica(Exception exception) {
		
		System.out.println("Erro genérico acontecendo...");
		exception.printStackTrace();
		
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", exception);
		
		return modelAndView;
	}
}
