Exercícios do curso Spring MVC 1 e 2 da Alura

* bootstrap 3.4.1
* Tomcat 7
* JDK 8
* MySQL Dialect 5 (not 57)
* MySQL Server 5.7.28 (downloaded installer)
* DBeaver connection with timezone America/Sao_Paulo
* Encoding da JSP: UTF-8
* Encoding da app: UTF-8 (setado via filtro do Spring)
* O caminho dos diretórios webapps, WEB-INF e arquivos-sumario é:

> eclipse-workplace/.metadata/.plugins/org.eclipse.wst.server.core/tmp1/wtpwebapps

ou seja, dentro do diretório do eclipse-workspace, e não dentro do tomcat ou do projeto